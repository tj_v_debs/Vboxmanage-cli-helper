#!/bin/bash

##TJ's VirtualBox Disk managment front end###

#Greeter
echo "Welcome to TJ's virtualbox manager front end"

#Raw disk access function
function func_raw_access {

	echo "You have selected raw disk access"
	read -rsn1 -p"Press any key to continue";echo
#lists disks and askk which disk the user wishes to create the raw vmdk for then sets their selection to var_raw_disk
	lsblk
	echo "Please select a source drive to access within virtualbox (sda, sdb, sdc, etc.)"

	read var_raw_disk

	echo "You have selected $var_raw_disk"

		read -rsn1 -p"Press any key to continue";echo
#Asks directory to save the raw vmdk to
	echo "Please select a directory to save the raw disk link to"
		read var_raw_disk_dir
	echo "You have selected $var_raw_disk_dir"
		read -rsn1 -p"Press any key to continue";echo
#Asks what the user wishes to name the raw vmdk
	echo "What do you wish to call this raw vmdk file?"
		read var_raw_disk_name

	echo "you have selected $var_raw_disk_name as the name of the raw vmdk file"
#Confirms users choice
	echo "Type yes if this is the correct target disk as well as the correct path and file name?"
	echo "Target Disk: /dev/$var_raw_disk"
	echo "path and file name: $var_raw_disk_dir/$var_raw_disk_name ?"
		read var_raw_confirmation
			if [[ $var_raw_confirmation == yes ]]; then
				VBoxManage internalcommands createrawvmdk -filename $var_raw_disk_dir/$var_raw_disk_name.vmdk -rawdisk /dev/$var_raw_disk
			else
				exit
			fi
	echo "All done, execute script again to do more"

}
#End OF RAW DISK FUNCTION

# CLONE DISK FUNCTION (VCLONE00)

function func_vbox_clone_disk {
		
		#User selects disk to clone then selects the format of the clone
		echo "you have selected to clone a virtual disk"
			read -rsn1 -p"Press any key to continue";echo
		echo "Please select a disk you wish to clone (type the full file path and name ex /home/user/virtualdisks/blah.vdi)"
			read var_clone_target
		echo "You have selected $var_clone_target as your target virtual disk to clone"
		
		#User selects location and name of clone
		echo "Please select path and name of cloned disk"
			read var_clone_name
		echo "you have selected $var_clone_name"
		echo "type yes if this is correct"
			if [[ var_vclone_confirmation == yes ]]; then
				echo "continuing"
			else
				echo "Cancelling operation"
			fi
			
		#Asks user which format they would like to use
		echo "Please select a format for cloned disk"
		echo " (1)VDI (2)VMDK (3)VHD (4)RAW (5)Quit"
			read n
				case $n in
				1) echo "You have selected VDI as your clone type"
					VBoxManage clonehd $var_clone_target $var_clone_name.vdi;;
					
				2) echo "You have selected VMDK as your clone type"
					VBoxManage clonehd $var_clone_target $var_clone_name.vmdk;;
				3) echo "You have selected VHD as your clone type"
					VBoxManage clonehd $var_clone_target $var_clone_name.vhd;;
				4) echo "You have selected RAW as your clone type"
					VBoxManage clonehd $var_clone_target $var_clone_name.raw;;
				5) echo "You picked quit :(" 
					exit;;
				esac
}


# MAIN MENU(MENU00) 
#Askss  user which function to execute.

function func_vbox_main_menu () {
echo "Welcome to TJ's virtualbox clone and raw disk access helper." 
echo "Please select what you would like to do"
echo "(1)Raw Disk Access (2)Virtual Disk Cloning (3)Quit"
	read main_n
		case $main_n in
			1) echo "you have selected raw disk access"
					func_raw_access;;
			2) echo "you have selected virtual disk cloning"
					func_vbox_clone_disk;;
			3) echo "Exiting :("
				exit;;
		esac				
	
}

func_vbox_main_menu
		
			
